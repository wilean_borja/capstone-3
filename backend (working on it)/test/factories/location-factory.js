const { factory } = require('factory-bot');
const casual = require('casual');

const Location = require('../../src/models/Location');

factory.define('location', Location, {
    name: () => casual._company_name(),
    location: () => casual._city()
});