const { factory } = require('factory-bot');
const casual = require('casual');

const Cinema = require('../../src/models/Cinema');

factory.define('cinema', Cinema, {
    name: () => casual._catch_phrase(),
    description: ()=> casual._short_description(),
    seats: () => casual._integer(from=20, to= 300),
    location: factory.assoc('location', 'id')
})