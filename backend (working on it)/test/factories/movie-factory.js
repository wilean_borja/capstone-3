const { factory } = require('factory-bot');
const casual = require('casual');

const Movie = require('../../src/models/Movie');

factory.define('movie', Movie, {
    title: () => casual._title(),
    director: () => casual._full_name(),
    artists: () => casual._array_of_words(n=3),
    coverImg: () => casual._word(),
    preview: () => casual._word(),
    summary: () => casual._description(),
    runTime: () => casual._integer( from=100, to=500 ),
    isShowing: () => casual._coin_flip()
});