const mongoose = require('mongoose');

const { factory, MongooseAdapter } = require('factory-bot');

require('../test/factories/movie-factory');
require('../test/factories/location-factory');
require('../test/factories/cinema-factory');

const config = require('../src/config');

// create a DB Connection

mongoose.connect(config.databaseURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

factory.setAdapter( new MongooseAdapter());

const seed = async() => {
    await factory.createMany('movie', 50);
    await factory.createMany('location', 10);
    await factory.createMany('cinema', 20);
}

console.log('Started seeding database...');

seed()
.then(()=>console.log('Finished seeding database'))
.catch((e)=> console.log(e))
.finally( () => mongoose.connection.close() )

