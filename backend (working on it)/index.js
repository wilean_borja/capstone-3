// call express
const express = require('express');

// create an instance of express
const app = express();

// call cors
const cors = require('cors');

// call mongoose
const mongoose = require('mongoose');

// call config file
const config = require('./src/config');

// call multer
const multer = require('multer');

// call path
const path = require('path'); 

// allow saving files in the public directory
app.use(express.static(path.join(__dirname, 'public')));

// allow parsing of data from url
app.use(express.urlencoded({extended:false}));

// allow parsing of data from a form
app.use(express.json());

// allow cors
app.use(cors());

// connect to database
mongoose.connect(config.databaseURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then( () => {
    console.log("Remote Database Connection Successful");
});

app.listen( config.PORT, () => {
    console.log(`Listening on Port ${config.PORT}`);
});

// location API
const locationRoutes = require('./src/routes/LocationRoutes');
app.use('/admin', locationRoutes);

// cinema API
const cinemaRoutes = require('./src/routes/CinemaRoutes');
app.use('/admin', cinemaRoutes);

// movie API
const movieRoutes = require('./src/routes/MovieRoutes');
app.use('/admin', movieRoutes);

// schedule API
const scheduleRoutes = require('./src/routes/ScheduleRoutes');
app.use('/admin', scheduleRoutes);

// booking API
const bookingRoutes = require('./src/routes/BookingRoutes');
app.use('/admin', bookingRoutes);

// auth API
const userRoutes = require('./src/routes/UserRoutes');
app.use('/', userRoutes);

// payment API
const paymentRoutes = require('./src/routes/PaymentRoutes');
app.use('/', paymentRoutes);


// for uploading of images

// define where to store
let storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, 'public/images/uploads');
    },
    filename: (req, file, callback) => {
        callback(null, Date.now() + '-' + file.originalname);
    }
});

const upload = multer({storage});

app.post('/upload', upload.single('image'), (req, res) => {
    if(req.file){
        res.json({ imageUrl: `images/uploads/${req.file.filename}`})
    }else{
        res.status(401).send('Bad Request. Please try again later.');
    }
});