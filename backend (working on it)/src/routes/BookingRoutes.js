const express = require("express");
const BookingRouter = express.Router();
const BookingModel = require("../models/Booking");
const ScheduleModel = require('../models/Schedule');

BookingRouter.post("/addbooking", async (req, res) => {
  try {
    let booking = BookingModel({
      schedule: req.body.scheduleId,
      user: req.body.userId,
      payment: req.body.payment,
      amount: req.body.amount,
    });
    booking = await booking.save();
    
    let schedule = await ScheduleModel.findById(req.body.scheduleId);
    let updatedSchedule = await ScheduleModel.findByIdAndUpdate(req.body.scheduleId, {seats : schedule.seats - 1}, { new: true });
    res.send(updatedSchedule);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});

BookingRouter.get("/bookings", async (req, res) => {
  try {
    const booking = await BookingModel.find().populate({
      path: 'schedule',
      populate:['movie', 'cinema']
    }, {
      path: 'user'
    });
    res.send(booking);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});

// get bookings by user ID
BookingRouter.get("/bookings/:id", async (req, res) => {
  try {
    const booking = await BookingModel.find({user: req.params.id}).populate({
      path: 'schedule',
      populate:['movie', 'cinema']
    });
    res.send(booking);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});

// cancel booking
BookingRouter.patch('/cancelbooking', async(req, res) => {
  try{
    const updates = {
      status: 'Cancelled',
      payment: 'Cancelled'
    }

    const booking = await BookingModel.findByIdAndUpdate(req.body.id, updates, { new: true });
    let schedule = await ScheduleModel.findById(req.body.scheduleId);
    let updatedSchedule = await ScheduleModel.findByIdAndUpdate(req.body.scheduleId, {seats : schedule.seats + 1}, { new: true });

    res.send(booking);

  }catch(e){
    res.status(401).send('Bad Request. Please try again.');
  }
})


BookingRouter.delete("/deletebooking", async (req, res) => {
  try {
    const booking = await BookingModel.findByIdAndDelete(req.body.id);
    res.send(booking);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});

BookingRouter.patch("/updatebooking", async (req, res) => {
  try {
    const updates = {
      schedule: req.body.scheduleId,
      user: req.body.userId,
      status: req.body.status,
      payment: req.body.payment,
      amount: req.body.amount,
    };

    let booking = await BookingModel.findByIdAndUpdate(req.body.id, updates, {
      new: true,
    });
    res.send(booking);
  } catch (e) {
    res.status(401).send("Bad Request. Please try again");
  }
});

module.exports = BookingRouter;