const express = require('express')

const MovieRouter = express.Router();

const MovieModel = require('../models/Movie')

// Add Movie
MovieRouter.post('/addmovie', async(req, res)=>{
    try{
        let movie = MovieModel ({
            title:req.body.title,
            director:req.body.director,
            artists:req.body.artists,
            coverImg:req.body.coverImg,
            preview:req.body.preview,
            summary:req.body.summary,
            runTime:req.body.runTime,
        });

        movie = await movie.save()
        res.send(movie)
    }catch(e){
        res.status(401).send('Bad Request.Please try again')
    }
});

// Get All Movies

MovieRouter.get('/movies/:offset',async(req,res)=>{
    try{
        const movies = await MovieModel.find();
        const selectedMovies = await MovieModel.find().skip(parseInt(req.params.offset) * 10).limit(10);
        res.send([movies, selectedMovies])
    }catch(e){
        res.status(401).send('bad Reuest, pls try again')
    }
})

// Movie Delete

MovieRouter.delete('/deletemovie',async(req,res)=>{
    try{
        const movie = await MovieModel.findByIdAndDelete(req.body.id);
        res.send(movie)
    }catch(e){
        res.status(401).send('bad request, Please Try again')

    }
})

// Update Movie
MovieRouter.patch('/updatemovie',async(req,res)=>{
    try{
        let updates ={
            title:req.body.title,
            director:req.body.director,
            artists:req.body.artists,
            coverImg:req.body.coverImg,
            preview:req.body.preview,
            summary:req.body.summary,
            runTime:req.body.runTime
        }
        let movie = await MovieModel.findByIdAndUpdate(req.body.id,updates,{new:true})
        res.send(movie)
    }catch(e){
        res.status(401).send('Bad Request. Please try again')
    }
});

MovieRouter.patch('/updateshowing', async (req, res) => {
    try{
        let update = { isShowing: req.body.isShowing }
        let movie = await MovieModel.findByIdAndUpdate(req.body.id, update, { new: true })
        res.send(movie);
    }catch(e){
        res.status(401).send('Bad Request. Please try again.');
    }
});

MovieRouter.patch('/updatecomingsoon', async ( req, res ) => {
    try{
        let update = { isComingSoon: req.body.isComingSoon }
        let movie = await MovieModel.findByIdAndUpdate(req.body.id, update, { new: true });
        res.send(movie);
    }catch(e){
        res.status(401).send('Bad Request. Please try again.');
    }
})


module.exports = MovieRouter