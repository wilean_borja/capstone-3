const express = require('express');

const PaymentRouter = express.Router();

const stripe = require('stripe')('sk_test_r2NeaSDULDjApNI4DJ0yuija00zetLJuHn');
const BookingModel = require('../models/Booking');

PaymentRouter.post('/charge', async(req, res) => {
    try{
        const customer = await stripe.customers.create({
            email: req.body.email,
            description: "Payment for Movie",
            source: "tok_visa"
        });
    
        let chargeResponse = await stripe.charges.create({
            amount: req.body.amount,
            currency: 'php',
            source: "tok_visa",
            description: "Payment for Movie"
        });

        await BookingModel.findByIdAndUpdate(req.body.id, { status: "Paid", payment: 'Stripe' });

        res.send(chargeResponse);
    }catch(e){
        console.log(e);
        res.status(401).send('Bad Request. Please try again.')
    }
})

module.exports = PaymentRouter;