const express = require('express');

const LocationRouter = express.Router();

const LocationModel = require('../models/Location');

const auth = require('../middlewares/auth_middleware');
const isAdmin = require('../middlewares/isAdmin_middleware');

// add Location
LocationRouter.post('/addlocation', isAdmin, async(req, res)=> {
    try{
        let location = LocationModel ({
            name: req.body.name,
            location: req.body.location
        });

        location = await location.save();
        res.send(location);
    }catch(e){
        res.status(401).send('Bad Request. Please try again');
    }
});

// get all location
LocationRouter.get('/locations', async(req, res) => {
    try {
        const locations = await LocationModel.find();
        res.send(locations);
    }catch(e){
        res.status(401).send('Bad Request. Please try again');
    }
});

// delete location
LocationRouter.delete('/deletelocation', auth, async(req, res) => {
    try {
        const location = await LocationModel.findByIdAndDelete(req.body.id);
        res.send(location);
    }catch(e){
        res.status(401).send('Bad Request. Please try again');
    }
});

LocationRouter.patch('/updatelocation', async(req,res) => {
    try {
        const updates = {
            name: req.body.name,
            location: req.body.location
        }

        let location = await LocationModel.findByIdAndUpdate(req.body.id, updates, { new: true});

        res.send(location);
    }catch(e){
        res.status(401).send('Bad Request. Please try again');
    }
})

module.exports = LocationRouter;