const jwt = require('jsonwebtoken');
const config = require('../config');

module.exports = (req, res, next) => {
    // const token = req.header('x-auth-token');

    const token = req.body.token;

    if(!token) return res.status(401).send('Bad request. Please try again.');

    try{
        let payload = jwt.verify(token, config.secret);
        req.user = payload;
        next();
    }catch(e){
        res.status(401).send('Unauthorized.');
    }
}
