import room1 from "./images/details-1.jpeg";
import room2 from "./images/details-2.jpeg";
import room3 from "./images/details-3.jpeg";
import room4 from "./images/details-4.jpeg";
import img1 from "./images/room-1.jpeg";
import img2 from "./images/room-2.jpeg";
import img3 from "./images/room-3.jpeg";
import img4 from "./images/room-4.jpeg";
import img5 from "./images/room-5.jpeg";
import img6 from "./images/room-6.jpeg";
import img7 from "./images/room-7.jpeg";
import img8 from "./images/room-8.jpeg";
import img9 from "./images/room-9.jpeg";
import img10 from "./images/room-10.jpeg";
import img11 from "./images/room-11.jpeg";
import img12 from "./images/room-12.jpeg";

export default [
  {
    sys: {
      id: "1"
    },
    fields: {
      name: "single economy",
      slug: "single-economy",
      type: "single",
      price: 2000,
      size: 200,
      capacity: 1,
      pets: false,
      breakfast: false,
      featured: false,
      description:
        "Our comfortable single rooms are just the right size if you are travelling alone. It is fully equipped with all comforts.",
      extras: [
        "Smart TV",
        "Air conditioning",
        "Universal sockets",
        "Free WiFi",
        "Fridge",
        "Hairdryer",
        "Rain shower",
        "Nespresso machine with free coffee and tea facilities"
      ],
      images: [
        {
          fields: {
            file: {
              url: img1
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room3
            }
          }
        },
        {
          fields: {
            file: {
              url: room4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "2"
    },
    fields: {
      name: "single basic",
      slug: "single-basic",
      type: "single",
      price: 2500,
      size: 250,
      capacity: 1,
      pets: false,
      breakfast: false,
      featured: false,
      description:
        "Our comfortable single rooms are just the right size if you are travelling alone. It is fully equipped with all comforts.",
      extras: [
        "Smart TV",
        "Air conditioning",
        "Universal sockets",
        "Free WiFi",
        "Fridge",
        "Hairdryer",
        "Rain shower",
        "Nespresso machine with free coffee and tea facilities"
      ],
      images: [
        {
          fields: {
            file: {
              url: img2
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room3
            }
          }
        },
        {
          fields: {
            file: {
              url: room4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "3"
    },
    fields: {
      name: "single standard",
      slug: "single-standard",
      type: "single",
      price: 2750,
      size: 300,
      capacity: 1,
      pets: true,
      breakfast: false,
      featured: false,
      description:
        "Our comfortable single rooms are just the right size if you are travelling alone. It is fully equipped with all comforts.",
      extras: [
        "Smart TV",
        "Air conditioning",
        "Universal sockets",
        "Free WiFi",
        "Fridge",
        "Hairdryer",
        "Rain shower",
        "Nespresso machine with free coffee and tea facilities"
      ],
      images: [
        {
          fields: {
            file: {
              url: img3
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room3
            }
          }
        },
        {
          fields: {
            file: {
              url: room4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "4"
    },
    fields: {
      name: "single deluxe",
      slug: "single-deluxe",
      type: "single",
      price: 3500,
      size: 400,
      capacity: 1,
      pets: true,
      breakfast: true,
      featured: false,
      description:
        "Our comfortable single rooms are just the right size if you are travelling alone. It is fully equipped with all comforts.",
      extras: [
        "Smart TV",
        "Air conditioning",
        "Universal sockets",
        "Free WiFi",
        "Fridge",
        "Hairdryer",
        "Rain shower",
        "Nespresso machine with free coffee and tea facilities"
      ],
      images: [
        {
          fields: {
            file: {
              url: img4
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room3
            }
          }
        },
        {
          fields: {
            file: {
              url: room4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "5"
    },
    fields: {
      name: "double economy",
      slug: "double-economy",
      type: "double",
      price: 3500,
      size: 300,
      capacity: 2,
      pets: false,
      breakfast: false,
      featured: false,
      description:
        "Our comfortable double rooms are just the right size if you are travelling with someone. It is fully equipped with all comforts.",
      extras: [
        "Alarm",
        "Wardrobe",
        "Minibar",
        "Air conditioning",
        "Safety Deposit Box",
        "Iron",
        "Electric kettle",
        "Sofa",
        "Desk",
        "Smart TV",
        "Telephone",
        "Cable Channels",
        "Universal sockets",
        "Wake-up service",
        "Free WiFi",
        "Fridge",
        "Hairdryer",
        "Rain shower",
        "Nespresso machine with free coffee and tea facilities"
      ],
      images: [
        {
          fields: {
            file: {
              url: img5
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room3
            }
          }
        },
        {
          fields: {
            file: {
              url: room4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "6"
    },
    fields: {
      name: "double basic",
      slug: "double-basic",
      type: "double",
      price: 3800,
      size: 350,
      capacity: 2,
      pets: false,
      breakfast: false,
      featured: false,
      description:
        "Our comfortable double rooms are just the right size if you are travelling with someone. It is fully equipped with all comforts.",
      extras: [
        "Alarm",
        "Wardrobe",
        "Minibar",
        "Air conditioning",
        "Safety Deposit Box",
        "Iron",
        "Electric kettle",
        "Sofa",
        "Desk",
        "Smart TV",
        "Telephone",
        "Cable Channels",
        "Universal sockets",
        "Wake-up service",
        "Free WiFi",
        "Fridge",
        "Hairdryer",
        "Rain shower",
        "Nespresso machine with free coffee and tea facilities"
      ],
      images: [
        {
          fields: {
            file: {
              url: img6
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room3
            }
          }
        },
        {
          fields: {
            file: {
              url: room4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "7"
    },
    fields: {
      name: "double standard",
      slug: "double-standard",
      type: "double",
      price: 4250,
      size: 400,
      capacity: 2,
      pets: true,
      breakfast: false,
      featured: false,
      description:
        "Our comfortable double rooms are just the right size if you are travelling with someone. It is fully equipped with all comforts.",
      extras: [
        "Alarm",
        "Wardrobe",
        "Minibar",
        "Air conditioning",
        "Safety Deposit Box",
        "Iron",
        "Electric kettle",
        "Sofa",
        "Desk",
        "Smart TV",
        "Telephone",
        "Cable Channels",
        "Universal sockets",
        "Wake-up service",
        "Free WiFi",
        "Fridge",
        "Hairdryer",
        "Rain shower",
        "Nespresso machine with free coffee and tea facilities"
      ],
      images: [
        {
          fields: {
            file: {
              url: img7
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room3
            }
          }
        },
        {
          fields: {
            file: {
              url: room4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "8"
    },
    fields: {
      name: "double deluxe",
      slug: "double-deluxe",
      type: "double",
      price: 5000,
      size: 500,
      capacity: 2,
      pets: true,
      breakfast: true,
      featured: true,
      description:
        "Our comfortable double rooms are just the right size if you are travelling with someone. It is fully equipped with all comforts.",
      extras: [
        "Alarm",
        "Wardrobe",
        "Minibar",
        "Air conditioning",
        "Safety Deposit Box",
        "Iron",
        "Electric kettle",
        "Sofa",
        "Desk",
        "Smart TV",
        "Telephone",
        "Cable Channels",
        "Universal sockets",
        "Wake-up service",
        "Free WiFi",
        "Fridge",
        "Hairdryer",
        "Rain shower",
        "Nespresso machine with free coffee and tea facilities"
      ],
      images: [
        {
          fields: {
            file: {
              url: img8
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room3
            }
          }
        },
        {
          fields: {
            file: {
              url: room4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "9"
    },
    fields: {
      name: "family economy",
      slug: "family-economy",
      type: "family",
      price: 6000,
      size: 500,
      capacity: 3,
      pets: false,
      breakfast: false,
      featured: false,
      description:
        "Our comfortable family rooms are just the right size if you are travelling with your family. It is fully equipped with all comforts.",
      extras: [
        "Plush pillows and breathable bed linens",
        "Soft, oversized bath towels",
        "Full-sized, pH-balanced toiletries",
        "Complimentary refreshments",
        "Enhanced security",
        "Memory-foam beds",
        "Alarm",
        "Wardrobe",
        "Minibar",
        "Air conditioning",
        "Safety Deposit Box",
        "Iron",
        "Electric kettle",
        "Sofa",
        "Desk",
        "Smart TV",
        "Telephone",
        "Cable Channels",
        "Universal sockets",
        "Wake-up service",
        "Free WiFi",
        "Fridge",
        "Hairdryer",
        "Rain shower",
        "Nespresso machine with free coffee and tea facilities"
      ],
      images: [
        {
          fields: {
            file: {
              url: img9
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room3
            }
          }
        },
        {
          fields: {
            file: {
              url: room4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "10"
    },
    fields: {
      name: "family basic",
      slug: "family-basic",
      type: "family",
      price: 6500,
      size: 550,
      capacity: 4,
      pets: false,
      breakfast: false,
      featured: false,
      description:
        "Our comfortable family rooms are just the right size if you are travelling with your family. It is fully equipped with all comforts.",
      extras: [
        "Plush pillows and breathable bed linens",
        "Soft, oversized bath towels",
        "Full-sized, pH-balanced toiletries",
        "Complimentary refreshments",
        "Enhanced security",
        "Memory-foam beds",
        "Alarm",
        "Wardrobe",
        "Minibar",
        "Air conditioning",
        "Safety Deposit Box",
        "Iron",
        "Electric kettle",
        "Sofa",
        "Desk",
        "Smart TV",
        "Telephone",
        "Cable Channels",
        "Universal sockets",
        "Wake-up service",
        "Free WiFi",
        "Fridge",
        "Hairdryer",
        "Rain shower",
        "Nespresso machine with free coffee and tea facilities"
      ],
      images: [
        {
          fields: {
            file: {
              url: img10
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room3
            }
          }
        },
        {
          fields: {
            file: {
              url: room4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "11"
    },
    fields: {
      name: "family standard",
      slug: "family-standard",
      type: "family",
      price: 7300,
      size: 600,
      capacity: 5,
      pets: true,
      breakfast: false,
      featured: false,
      description:
        "Our comfortable family rooms are just the right size if you are travelling with your family. It is fully equipped with all comforts.",
      extras: [
        "Plush pillows and breathable bed linens",
        "Soft, oversized bath towels",
        "Full-sized, pH-balanced toiletries",
        "Complimentary refreshments",
        "Enhanced security",
        "Memory-foam beds",
        "Alarm",
        "Wardrobe",
        "Minibar",
        "Air conditioning",
        "Safety Deposit Box",
        "Iron",
        "Electric kettle",
        "Sofa",
        "Desk",
        "Smart TV",
        "Telephone",
        "Cable Channels",
        "Universal sockets",
        "Wake-up service",
        "Free WiFi",
        "Fridge",
        "Hairdryer",
        "Rain shower",
        "Nespresso machine with free coffee and tea facilities"
      ],
      images: [
        {
          fields: {
            file: {
              url: img11
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room3
            }
          }
        },
        {
          fields: {
            file: {
              url: room4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "12"
    },
    fields: {
      name: "family deluxe",
      slug: "family-deluxe",
      type: "family",
      price: 8500,
      size: 700,
      capacity: 6,
      pets: true,
      breakfast: true,
      featured: true,
      description:
        "Our comfortable family rooms are just the right size if you are travelling with your family. It is fully equipped with all comforts.",
      extras: [
        "Plush pillows and breathable bed linens",
        "Soft, oversized bath towels",
        "Full-sized, pH-balanced toiletries",
        "Complimentary refreshments",
        "Enhanced security",
        "Memory-foam beds",
        "Alarm",
        "Wardrobe",
        "Minibar",
        "Air conditioning",
        "Safety Deposit Box",
        "Iron",
        "Electric kettle",
        "Sofa",
        "Desk",
        "Smart TV",
        "Telephone",
        "Cable Channels",
        "Universal sockets",
        "Wake-up service",
        "Free WiFi",
        "Fridge",
        "Hairdryer",
        "Rain shower",
        "Nespresso machine with free coffee and tea facilities"
      ],
      images: [
        {
          fields: {
            file: {
              url: img12
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room3
            }
          }
        },
        {
          fields: {
            file: {
              url: room4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "13"
    },
    fields: {
      name: "vip",
      slug: "vip-room",
      type: "vip",
      price: 15000,
      size: 1000,
      capacity: 10,
      pets: true,
      breakfast: true,
      featured: true,
      description:
        "Our VIP room is designed with class and comfort to offer top standard accommodation to those seeking bigger space, king size bed, affordable luxury and maximum comfort. Its luminous design evokes the feeling of warmth and luxury. There is a cozy seating area with upholstered lounge sofa and table where guests can relax with a nice flat screen television and DVD player with variety of Hollywood movies. For added convenience, there is a table with a work/computer desk equipped with desktop, monitor and in-room internet access Wi-Fi for personal or business use.",
      extras: [
          "Plush pillows and breathable bed linens",
          "Soft, oversized bath towels",
          "Full-sized, pH-balanced toiletries",
          "Complimentary refreshments",
          "Enhanced security",
          "Memory-foam beds",
          "Alarm",
          "Wardrobe",
          "Minibar",
          "Air conditioning",
          "Safety Deposit Box",
          "Iron",
          "Electric kettle",
          "Sofa",
          "Desk",
          "Smart TV",
          "Telephone",
          "Cable Channels",
          "Universal sockets",
          "Wake-up service",
          "Free WiFi",
          "Fridge",
          "Hairdryer",
          "Rain shower",
          "Nespresso machine with free coffee and tea facilities",
          "Executive lounge access",
          "Personal gazebo"
        ],
      images: [
        {
          fields: {
            file: {
              url: room1
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room3
            }
          }
        },
        {
          fields: {
            file: {
              url: room4
            }
          }
        }
      ]
    }
  }
];
