import React, { Component } from "react";
import { FaWifi, FaDog, FaParking, FaSwimmingPool } from "react-icons/fa";
import Title from "./Title";
export default class Services extends Component {
  state = {
    services: [
      {
        icon: <FaWifi />,
        title: "Free Wi-Fi",
        info:
          "WiFi is available in all areas and is free of charge."
      },
      {
        icon: <FaDog />,
        title: "Pet friendly",
        info:
          "Pets are allowed on request. Charges may be applicable."
      },
      {
        icon: <FaParking />,
        title: "Free Shuttle",
        info:
          "Free private parking is possible on site (reservation is not needed)."
      },
      {
        icon: <FaSwimmingPool />,
        title: "Strongest Beer",
        info:
          "Swimming pool open 24/7 with lifeguard."
      }
    ]
  };
  render() {
    return (
      <section className="services">
        <Title title="services" />
        <div className="services-center">
          {this.state.services.map(item => {
            return (
              <article key={`item-${item.title}`} className="service">
                <span>{item.icon}</span>
                <h6>{item.title}</h6>
                <p>{item.info}</p>
              </article>
            );
          })}
        </div>
      </section>
    );
  }
}
